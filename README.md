<!-- BANNER -->
<div align='center'>
<img src="assets/banner.png" align=center height=256px>
</div>

<!-- SUBTITLE -->
<div align='center'>
<img src="https://img.shields.io/maintenance/yes/2021?color=3a86ff&style=flat-square" height=25px>
</div>



<br>



<!-- DESCRIPTION -->
<div align='left'>
<img src="assets/description.png" align=center height=125px>
</div>
<br>

**[Awesome](https://awesomewm.org/)** is a dynamic [window manager](https://wiki.archlinux.org/title/Window_manager) for X11.<br>
Is the most extensive, fully featured and complete there is, in my opinion.<br>
You can customize it to fit precisely your needs, if you know how to program.<br>
It's also written from scratch in C and configured with lua.<br>
[read more..](https://gitlab.com/yowls/awesomewm-rice/-/wikis/home)

-- Custom sets of globals

read_globals = {
	-- awesome API
	"awesome",
	"button",
	"dbus",
	"drawable",
	"drawin",
	"key",
	"keygrabber",
	"mousegrabber",
	"root",
	"selection",
	"tag",
	"window"
}

globals = {
	-- awesome API
	"screen",
	"mouse",
	"client"
}

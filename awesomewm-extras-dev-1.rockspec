package = "awesomewm-extras"
version = "dev-1"
source = {
   url = "git+ssh://git@gitlab.com/yowls/awesomewm-extras.git"
}
description = {
   detailed = "Extras features for the Awesome window manager",
   homepage = "https://gitlab.com/yowls/awesomewm-extras#readme",
   license = "LGPLv2"
}
build = {
   type = "builtin",
   modules = {
      ["color.apps.init"] = "color/apps/init.lua",
      ["color.borders.init"] = "color/borders/init.lua",
      ["color.init"] = "color/init.lua",
      init = "init.lua",
      settings = "settings.lua",
      ["signals.brightness"] = "signals/brightness.lua",
      ["signals.init"] = "signals/init.lua",
      ["signals.screenshot"] = "signals/screenshot.lua",
      ["signals.volume"] = "signals/volume.lua",
      ["widgets.osd"] = "widgets/osd.lua"
   }
}

#!/bin/sh
[ "${{TERM:-none}}" = "linux" ] && \
	printf '%b' '\e]P0{color0}
		\e]P8{color0}
		\e]P1{color1}
		\e]P9{color1}
		\e]P2{color2}
		\e]PA{color2}
		\e]P3{color3}
		\e]PB{color3}
		\e]P4{color4}
		\e]PC{color4}
		\e]P5{color5}
		\e]PD{color5}
		\e]P6{color6}
		\e]PE{color6}
		\e]P7{color7}
		\e]PF{color7}
		\ec'

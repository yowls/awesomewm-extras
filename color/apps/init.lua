-- Read templates and apply color scheme
-- then write it to a file

local beautiful = require('beautiful')
local cs = beautiful.cs

local paths = require("settings.paths")
local debug = require("library.helpers.debug")

local template_dir = paths.awesome .. "library/autostart/color_apps/"
local location_dir = paths.awesome_cache


local function colorize (template, new_file)
	-- read template file
	local f = io.open(template, "r")

	-- check file was opened correctly
	if not f then
		debug.log("Cannot open " .. template)
		debug.notify("Color apps", "Cannot open " .. template, "critical")
		return
	end

	local content = f:read("*all")
	f:close()

	-- edit template file applying color scheme
	local changes = string.gsub(content, "{background}", cs.bg)
	changes = string.gsub(changes, "{background_inactive}", cs.bgi)
	changes = string.gsub(changes, "{foreground}", cs.fg)
	changes = string.gsub(changes, "{foreground_inactive}", cs.fgi)
	changes = string.gsub(changes, "{accent}", cs.accent)
	changes = string.gsub(changes, "{alert}", cs.alert)
	changes = string.gsub(changes, "{color0}", cs.fgi)
	changes = string.gsub(changes, "{color1}", cs.color1)
	changes = string.gsub(changes, "{color2}", cs.color2)
	changes = string.gsub(changes, "{color3}", cs.color3)
	changes = string.gsub(changes, "{color4}", cs.color4)
	changes = string.gsub(changes, "{color5}", cs.color5)
	changes = string.gsub(changes, "{color6}", cs.color6)
	changes = string.gsub(changes, "{color7}", cs.fg)

	-- write changes to a new file
	local ff = io.open(new_file, "w")

	-- check file was opened correctly
	if not ff then
		debug.log("Cannot open " .. new_file)
		debug.notify("Color apps", "Cannot open " .. new_file, "critical")
		return
	end

	ff:write(changes)
	ff:close()
end


local function apply_xresources ()
	local xresources_template = template_dir .. "template.Xresources"
	local destination_xresources = location_dir .. "awesome.Xresources"

	-- Create file based on template
	colorize(xresources_template, destination_xresources)

	-- Merge new settings
	local cmd = "xrdb -merge -quiet " .. destination_xresources
	os.execute(cmd)
end


local function apply_css ()
	local css_template = template_dir .. "template.css"
	local destination_css = location_dir .. "awesome.css"

	local scss_template = template_dir .. "template.scss"
	local destination_scss = location_dir .. "awesome.scss"

	-- Create file based on template
	colorize(css_template, destination_css)
	colorize(scss_template, destination_scss)

	-- Merge new settings:
	-- need to import the file in your custom css like:
	-- @import url('file:///home/user/.cache/awesome/awesome.css');
end


local function apply_json ()
	local json_template = template_dir .. "template.json"
	local destination_json = location_dir .. "awesome.json"

	-- Create file based on template
	colorize(json_template, destination_json)

	-- Apply color scheme:
	-- import the file
end


local function apply_kitty ()
	local kitty_template = template_dir .. "template.kitty.conf"
	local destination_kitty = location_dir .. "awesome.kitty.conf"

	-- Create file based on template
	colorize(kitty_template, destination_kitty)

	-- Merge new settings:
	-- need to add in your kitty.conf
	-- include ~/.cache/awesome/awesome.kitty.conf
end


local function apply_rofi ()
	local rofi_template = template_dir .. "template.rasi"
	local destination_rofi = location_dir .. "awesome.rasi"

	-- Create file based on template
	colorize(rofi_template, destination_rofi)

	-- Apply color scheme:
	-- nedd to add in your config.rasi
	-- @import "~/.cache/awesome/awesome.rasi"
end


-- TODO: apply_tty


-- MAIN
apply_xresources()
apply_css()
apply_json()
apply_kitty()
apply_rofi()

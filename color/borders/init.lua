---
-- Set different border color
-- for clients
---

local beautiful = require('beautiful')


-- get color from beautiful lib
local function random_beautiful_color()
	return beautiful.cs[ math.random() ]
end


-- get color from a near border color
local function random_near_color(color)
	local s1 = string.gsub()
	local s2 = string.gsub()
	local s3 = string.gsub()

	return "#" .. s1 .. s2 .. s3
end


-- Create signal for handle new border colors
client.connect_signal("manage", function (c)
	local acolor = random_beautiful_color()
	c.border_color = acolor
end)

# 📖 CHANGELOG
Log of major code changes

<br>
<br>

## [0.1.0] 202X-XX-XX (WIP)
### ✨ Added
+ Color
  - Applications
+ Signals
  - OSD Volume notification
  - OSD Brightnes notification

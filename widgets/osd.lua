---
-- OSD NOTIFICATION WIDGET
---

local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi

-- TODO: add mouse buttons
-- TODO: add tooltip
-- TODO: add popup
-- TODO: manage slider color by parameters


--- PREVIEW
-- |----------------------|
-- |                      |
-- |         ICON         |
-- |                      |
-- |----------------------|
-- |    (PROGRESS BAR)    |
-- |      TEXT VALUE      |
-- |----------------------|


-- MAIN
local widget = {}
local function Osd(args)

	-- Settings
	local settings	= args or {}
	local icon		= settings.icon		or beautiful.awesome_icon
	local font		= settings.font		or beautiful.font
	local margin	= settings.margin	or 0
	local value		= settings.value	or 0
	local signal	= settings.signal	or "osd_signal"

	-- =======================================

	-- Create the widget
	local widget_icon = wibox.widget {
		{
			id		= "icon",
			image	= gears.color.recolor_image(icon, beautiful.fg_normal),
			resize	= true,
			widget	= wibox.widget.imagebox
		},
		valigh = 'center',
		layout = wibox.container.place
	}

	local widget_text = wibox.widget {
		text	= tostring(value) .. "%",
		font	= font,
		align	= "center",
		valign	= "center",
		widget	= wibox.widget.textbox
	}

	local widget_slider = wibox.widget {
		bar_height			= dpi(20),
		bar_shape			= gears.shape.rounded_rect,
		bar_color			= beautiful.cs.fgi .. "66",
		bar_active_color	= beautiful.cs.accent,
		handle_color		= beautiful.cs.accent,
		handle_width		= dpi(20),
		handle_shape		= gears.shape.circle,
		value				= value,
		maximum				= 100,
		widget				= wibox.widget.slider
	}

	-- Update value with the slider
	-- widget_slider:connect_signal('property::value', function(_, new_value)
	--     -- TODO: spawn cmd
	--     print(new_value)
	-- end)

	-- =======================================

	widget = wibox.widget {
		{
			{
				nil,
				widget_icon,
				nil,
				expand = "none",
				layout = wibox.layout.fixed.horizontal
			},
			{
				widget_slider,
				widget_text,
				spacing = dpi(10),
				layout = wibox.layout.flex.vertical
			},
			layout = wibox.layout.fixed.vertical
		},
		left	= margin,
		right	= margin,
		widget	= wibox.container.margin
	}

	-- =======================================

	awesome.connect_signal(signal, function(new_value)
		widget_slider:set_value(new_value)
		widget_text.text = tostring(new_value) .. "%"
	end)

	-- =======================================

	-- TODO: use self
	-- function widget:update ()
	-- end

	-- =======================================

	-- Signals
	-- widget:connect_signal('button::press', function()
	-- widget:connect_signal('mouse::enter', function()
	-- widget:connect_signal('mouse::leave', function()

	return widget
end


-- Return widget
return setmetatable(widget, {
	__call = function(_, ...)
		return Osd(...)
	end
})

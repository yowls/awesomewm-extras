---
-- SHOW VOLUME IN A OSD NOTIFCATION
---

local awful = require('awful')
local gears = require('gears')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi

local osd_widget = require('extras.widgets.osd')

-- TODO: add mute indicator
-- TODO: update icon based on volume %
-- TODO: hide others OSD when showing this one
-- TODO: connect 'system::volume:up/down' and send 'system::volume'


local function get_volume()
	local cmd = io.popen("amixer sget Master | grep 'Right:' | awk -F'[][]' '{ print $2 }' | sed 's/.$//'")
	local cmd_out = cmd:read("*a")
	cmd:close()

	return tonumber( string.gsub(cmd_out, "\n", ""), 10 )
end

local function volume_up()
	awful.spawn('amixer -q sset Master 5%+')
end

local function volume_down()
	awful.spawn('amixer -q sset Master 5%-')
end

local function get_mute_status()
	local cmd = io.popen("pacmd list-sinks | awk '/muted/ { print $2 }'")
	local cmd_out = cmd:read("*a")
	cmd_out = string.gsub(cmd_out, "\n", "")
	cmd:close()

	if string.find(cmd_out, "yes") then
		return true		-- muted
	else
		return false	-- unmuted
	end
end

local function volume_toggle()
	awful.spawn('amixer -q sset Master toggle')
end


local timer = gears.timer {
	timeout = 2,
	-- autostart = true,
	callback  = function()
		local focused = awful.screen.focused()
		focused.osd_volume.visible = false
	end
}


-- MAIN
return function (s)

	-- settings
	local height	= dpi(240)
	local width		= dpi(240)
	local offset	= dpi(5)
	local margin	= dpi(25)

	-- ===================

	-- local current_vol = get_volume()
	local current_vol = 75

	-- build widget
	local widget = osd_widget {
		icon	= beautiful.icons.system.volume,
		font	= beautiful.font,
		margin	= margin,
		value	= current_vol,
		signal	= "system::volume:set",
	}

	-- ===================

	awesome.connect_signal('system::volume:up', function()
		-- TODO: unmute if is muted
		volume_up()
		local value = get_volume()

		-- show the popup if is hidden
		s.osd_volume.visible = true
		if timer.started then
			timer:again()
		else
			-- s.osd_brightness.visible = false
			timer:start()
		end

		awesome.emit_signal('system::volume:set', value)
	end)

	awesome.connect_signal('system::volume:down', function()
		-- TODO: unmute if is muted
		volume_down()
		local value = get_volume()

		-- show the popup if is hidden
		s.osd_volume.visible = true
		if timer.started then
			timer:again()
		else
			timer:start()
		end

		awesome.emit_signal('system::volume:set', value)
	end)

	awesome.connect_signal('system::volume:toggle', function()
		volume_toggle()
		local value

		if get_mute_status() then
			value = get_volume()
		else
			value = 0
		end

		-- show the popup if is hidden
		s.osd_volume.visible = true
		if timer.started then
			timer:again()
		else
			timer:start()
		end

		awesome.emit_signal('system::volume:set', value)
	end)

	-- ===================

	-- return popup widget
	return awful.popup {
		screen			= s,
		ontop			= true,
		visible			= false,
		type			= "notification",
		height			= dpi(height),
		maximum_height	= dpi(height),
		width			= dpi(width),
		maximum_width	= dpi(width),
		offset			= dpi(offset),
		placement		= awful.placement.centered,
		shape			= gears.shape.rounded_rect,
		bg				= beautiful.bg_normal,
		widget			= widget
	}
end

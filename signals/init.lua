---
-- START ALL SIGNAL MODULES
---

screen.connect_signal("request::desktop_decoration", function(s)
	-- Show some system information in a OSD notification
	s.osd_volume		= require('extras.signals.volume')(s)
	s.osd_brightness	= require('extras.signals.brightness')(s)
	-- s.osd_screenshot	= require('extras.signals.screenshot')()
end)

---
-- SHOW BRIGHTNESS IN A OSD NOTIFCATION
---

local awful = require('awful')
local gears = require('gears')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi

local osd_widget = require('extras.widgets.osd')

-- TODO: update icon based on brightness %
-- TODO: hide others OSD when showing this one
-- TODO: connect 'system::brightness:up/down' and send 'system::brightness'


local function get_brightness()
	local cmd = io.popen("light -G")
	local cmd_out = cmd:read("*a")	-- get float
	cmd:close()
	return math.ceil(cmd_out)
end

local function brightness_up()
	awful.spawn("light -A 5")
end

local function brightness_down()
	awful.spawn("light -U 5")
end


local timer = gears.timer {
	timeout = 2,
	-- autostart = true,
	callback  = function()
		local focused = awful.screen.focused()
		focused.osd_brightness.visible = false
	end
}


-- MAIN
return function (s)

	-- settings
	local height	= dpi(200)
	local width		= dpi(200)
	local offset	= dpi(10)
	local margin	= dpi(20)

	-- ===================

	-- local current_brightness = get_brightness()
	local current_brightness = 15

	-- build widget
	local widget = osd_widget {
		icon	= beautiful.icons.system.brightness,
		font	= beautiful.font,
		margin	= margin,
		value	= current_brightness,
		signal	= "system::brightness:set"
	}

	-- ===================

	awesome.connect_signal('system::brightness:up', function()
		brightness_up()
		local value = get_brightness()

		-- show the popup if is hidden
		s.osd_brightness.visible = true
		if timer.started then
			timer:again()
		else
			-- s.osd_volume.visible = false
			timer:start()
		end

		awesome.emit_signal('system::brightness:set', value)
	end)

	awesome.connect_signal('system::brightness:down', function()
		brightness_down()
		local value = get_brightness()

		-- show the popup if is hidden
		s.osd_brightness.visible = true
		if timer.started then
			timer:again()
		else
			timer:start()
		end

		awesome.emit_signal('system::brightness:set', value)
	end)

	-- ===================

	-- return popup widget
	return awful.popup {
		screen			= s,
		ontop			= true,
		visible			= false,
		type			= "notification",
		height			= dpi(height),
		maximum_height	= dpi(height),
		width			= dpi(width),
		maximum_width	= dpi(width),
		offset			= dpi(offset),
		placement		= awful.placement.centered,
		shape			= gears.shape.rounded_rect,
		bg				= beautiful.bg_normal,
		widget			= widget
	}
end

--[[
.   ______     __     __     ______     ______     ______     __    __     ______
.  /\  __ \   /\ \  _ \ \   /\  ___\   /\  ___\   /\  __ \   /\ "-./  \   /\  ___\
.  \ \  __ \  \ \ \/ ".\ \  \ \  __\   \ \___  \  \ \ \/\ \  \ \ \-./\ \  \ \  __\
.   \ \_\ \_\  \ \__/".~\_\  \ \_____\  \/\_____\  \ \_____\  \ \_\ \ \_\  \ \_____\
.    \/_/\/_/   \/_/   \/_/   \/_____/   \/_____/   \/_____/   \/_/  \/_/   \/_____/
.      gitlab.com/yowls/awesomewm-extras
--]]

local extras = {}

--- START MODULES
function extras.init()
	require("extras.color")
	require("extras.signals")
	-- require("extras.daemons")
end

return extras
